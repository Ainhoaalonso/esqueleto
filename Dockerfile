#Imagen base
FROM node:latest
#Directorio de la app
WORKDIR /app
#Copio archivos. (Todos los que están a nivel de Dockerfile)
ADD . /app
#Dependencias
#RUN npm install
#Puerto
EXPOSE 3000
#Comando
CMD ["npm", "start"]
